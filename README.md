# TDT4250 - Advanced Software Design

Contributor: Vegard Sporstøl

<!-- TABLE OF CONTENTS -->
## Table of Contents

* [About the Project](#about-the-project)
    * [Case Description](#case-description)
* [Getting Started](#getting-started)
  * [Clone](#clone)
  * [Prerequisites](#prerequisites)
* [Class diagram of the model](#class-diagram-of-the-model)
* [Changes to the model from exercise 1 to 2](#changes-to-the-model-from-exercise-1-to-2)
* [How to run the transformation](#how-to-run-the-transformation)
* [Repo Structure](#repo-structure)


<!-- ABOUT THE PROJECT -->
## About The Project

This project is done as a part of the NTNU course TDT4250 - Advanced Software Design. This project will cover two different assignments:

* Exercise 1 - Modelling with Ecore
* Exercise 2 - Transforming a model instance

<!-- CASE DESCRIPTION -->
### Case Description
This case description provides a context for several assignments, where the overall goal is to explore the various features of EMF in general and Ecore in particular. The domain is the structure of study programs, i.e. which courses that are available (mandatory and electives) in which semesters and what specialisations are available. Details of each program can be found on corresponding pages, e.g. the 5 year Computer Science programme (MTDT), the 2 year Computer Science programme (MIDT) and 2 year Informatics programme.

**Courses**

The main identifying features of courses are their code and name, while the credits and level are important for their role in the programmes. Although the example programmes only have 7.5 credit courses, 5 and 10 credits are also common.

Most courses are part of several programmes, and can even be present several places in the same programme, e.g. be mandatory for one specialisation and elective in another one.

**Programmes**

Programmes can have varying number of years, and thus, semesters. Bachelor programmes are 3 year, most master programmes 2 years and integrated master programmes 5 years. Each year consists of courses worth 60 credits in total, and usually these are distributed evenly across the two semesters (but technically courses may span semesters).

Semesters may have open slots with constraints on which electives can be selected among when filling them. Generally you select a specific number of courses from a group of courses. In some cases, the group of courses to select among is not explicitly specified, instead criteria are given, e.g. a course must be at a certain level from (or outside) a specific field.

In addition to rules for a slot, there are also rules for combinations, e.g. at the master level you can select some, but not more that 22.5 credits (e.g. three 7.5 credit courses) from the 3rd level, the rest must be master (higher) level courses.

**Specialisations**

At certain points in a programme, students may choose one of several specialisations. The 5 year CS programme has such choice points in the 7th and 9th semesters, while the 2 year CS and Informatics programmes have the choice in the 1st semester. Note that not all specialisation chosen in the 7th semester offer another choice in the 9th, e.g. the AI does not offer any further choice (other than courses).

Individual study plans (optional)

The programmes define the possibilities (options) and limitations constraints, while individuel study plans is what a specific student has actually chosen. This is what is represented in each student's studweb page. In addition, the exam attempts and results are represented there.

<!-- GETTING STARTED -->
## Getting Started

The following section will cover how to clone and build the project

<!-- CLONE -->
### Clone

The repo must be clone through git using this link: https://gitlab.stud.idi.ntnu.no/vegarms/tdt4250-2020.git

### Prerequisites
You need to have the latest versions of Java (8.0.261) and Eclipse (2020-06)

**The following plug-ins must be installed:**

From the standard software site (select Eclipse 2020-06 in drop-down) and with Group Items by Category checked
* Acceleo - model to text transformation (M2T) and OCL interpreter view
* Ecore Diagram Editor (SDK) - editor for ecore models as diagram
* EMF Forms SDK - forms for ecore models
* Sirius Specifier Environment - diagram-based DSL framework
* Sirius Properties Views - Specifier Support
* Mylyn WikiText - editor for various wiki markup formats, including markdown

From the standard software site (select Eclipse 2020-06 in drop-down) and with Group Items by Category un-checked:
* Acceleo Query SDK - OCL implementation

From the software site http://hallvard.github.io/plantuml (type into text field):
* PlantUML Ecore Feature and PlantUML Feature (under PlantUML Eclipse support)
* PlantUML Library Feature (under PlantUML Library)

In addition, install the graphviz command line application and register its path to dot executable in the PlantUML preferences in Eclipse. 
The Eclipse PlantUML plugin is incompatible with the latest graphviz version, so use v2.38.


<!-- Class diagram of the model -->
## Class diagram of the model
(https://gitlab.stud.idi.ntnu.no/vegarms/tdt4250-2020/-/blob/master/tdt4250.sp.model/model/sp_class_diagram.png)

![](https://gitlab.stud.idi.ntnu.no/vegarms/tdt4250-2020/-/blob/master/tdt4250.sp.model/model/sp_class_diagram.png)


<!-- Changes to the model from exercise 1 to 2 -->
## Changes to the model from exercise 1 to 2
* Added some methods to link Studyplan, Specialization and Semesters
* Some minor changes to naming of derived feature and manually written code for constrains

<!-- How to run the transformation -->
## How to run the transformation
1. Clone the repo
2. Run the generate.mtl file as an Acceleo Application with this configuration:
    * **Project:** tdt4250.sp.acceleo.m2t
    * **Main class:** tdt2450.sp.acceleo.m2t.main.Generate
    * **Model:** /tdt4250.sp.model/model/University.xmi
    * **Target:** /tdt4250.sp.acceleo.m2t/src/tdt4250/sp/acceleo/m2t/main
    * **Runner:** Java Application
3. The taget file will with this setup be created in the same file as the generate file, and with this istance be named NTNU.html


<!-- Repo Structure -->
## Repo Structure
* tdt4250.sp.acceleo.m2t - *contains activator and generator files for the M2T transformation of the model using Acceleo. Target file is also here*
* tdt4250.sp.editor
* tdt4250.sp.edit
* tdt4250.sp.model.tests - *contains all test code for testing manually written code and constraints*
* tdt4250.sp.model
    * /model
        * /sp.ecore - *ecore model which illustrates the case with use of classes, datatypes, attributes, references and constraints with OCL* 
        * /University.xmi - *one instance of the model*
        * /sp.genmodel - *genmodel used to generate code*
    * /src/tdt4250/sp - *contains all source code for the project*
        * /impl 
            * /SemesterImpl.java - *contains implementations of one method and one derived feature*
        * /util 
            * /SpValidator.java - *contains manually written code for validation of constraints*

