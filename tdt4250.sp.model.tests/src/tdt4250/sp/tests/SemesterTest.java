/**
 */
package tdt4250.sp.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;
import tdt4250.sp.Course;
import tdt4250.sp.Semester;
import tdt4250.sp.SpFactory;
import tdt4250.sp.Specialization;
import tdt4250.sp.StudyPlan;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Semester</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are tested:
 * <ul>
 *   <li>{@link tdt4250.sp.Semester#getName() <em>Name</em>}</li>
 * </ul>
 * </p>
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link tdt4250.sp.Semester#addCourse(tdt4250.sp.Course) <em>Add Course</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class SemesterTest extends TestCase {

	/**
	 * The fixture for this Semester test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Semester fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(SemesterTest.class);
	}

	/**
	 * Constructs a new Semester test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SemesterTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Semester test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(Semester fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Semester test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Semester getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(SpFactory.eINSTANCE.createSemester());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link tdt4250.sp.Semester#getName() <em>Name</em>}' feature getter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.sp.Semester#getName()
	 * @generated NOT
	 */
	public void testGetName() {
		StudyPlan studyPlan = SpFactory.eINSTANCE.createStudyPlan();
		studyPlan.setStudentName("Vegard");
		
		Semester semester = SpFactory.eINSTANCE.createSemester();
		semester.setSemesterNumber(1);
		studyPlan.addSemester(semester);
		
		Specialization specialization = SpFactory.eINSTANCE.createSpecialization();
		specialization.setName("Datateknologi");
		studyPlan.chooseSpecialization(specialization);
		
		assertEquals("Datateknologi semester 1", semester.getName());
	}

	/**
	 * Tests the '{@link tdt4250.sp.Semester#addCourse(tdt4250.sp.Course) <em>Add Course</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.sp.Semester#addCourse(tdt4250.sp.Course)
	 * @generated NOT
	 */
	public void testAddCourse__Course() {
		Semester semester = SpFactory.eINSTANCE.createSemester();
		
		Course course = SpFactory.eINSTANCE.createCourse();
		course.setCode("TDT4250");
		course.setCredit(7.5f);
		course.setName("Avansert Programvaredesign");
		course.setLevel(3);
		
		semester.addCourse(course);
		
		assertTrue(semester.getCourse().contains(course));
		
	}

} //SemesterTest
