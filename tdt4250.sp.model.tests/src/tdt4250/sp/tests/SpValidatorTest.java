package tdt4250.sp.tests;

import org.eclipse.acceleo.query.delegates.AQLValidationDelegate;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EValidator.ValidationDelegate;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import junit.framework.TestCase;
import tdt4250.sp.Semester;
import tdt4250.sp.SpPackage;
import tdt4250.sp.StudyPlan;
import tdt4250.sp.University;

public class SpValidatorTest extends TestCase {
	
	private Resource testInstance;
	private Diagnostic diagnostics;
	
	protected Resource loadResource(String name) {
		ResourceSet resSet = new ResourceSetImpl();
		// relate the RaPackage identifier used in XMI files to the RaPackage instance (EPackage meta-object) 
		resSet.getPackageRegistry().put(SpPackage.eNS_URI, SpPackage.eINSTANCE);
		// relate the XMI parser to the 'xmi' file extension
		resSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("xmi", new XMIResourceFactoryImpl());
		
		return resSet.getResource(URI.createURI(SpValidatorTest.class.getResource(name + ".xmi").toString()), true);
	}

	protected void setUp() throws Exception {
		// register AQL (an OCL implementation) constraint support
		ValidationDelegate.Registry.INSTANCE.put("http://www.eclipse.org/acceleo/query/1.0", new AQLValidationDelegate());

		testInstance = loadResource("SpValidatorTest");
		diagnostics = Diagnostician.INSTANCE.validate(testInstance.getContents().get(0));
	}
	
	private Diagnostic findDiagnostics(Diagnostic diagnostic, Object o, String constraint) {
		if (diagnostic.getMessage().contains(constraint) && (o == null || diagnostic.getData().contains(o))) {
			return diagnostic;
		}
		for (Diagnostic child : diagnostic.getChildren()) {
			Diagnostic found = findDiagnostics(child, o, constraint);
			if (found != null) {
				return found;
			}
		}
		return null;
	}

	public void testNeedsEnoughCourses_valid() {
		University ntnu = (University) testInstance.getContents().get(0);
		StudyPlan studyplan = (StudyPlan) ntnu.getStudyPlan().get(0);
		Semester semester1 = (Semester) studyplan.getSemester().get(0);
		assertNull(findDiagnostics(diagnostics, semester1, "needsEnoughCourses"));
	}
	
	public void testNeedsEnoughCourses_invalid() {
		University ntnu = (University) testInstance.getContents().get(0);
		StudyPlan studyplan = (StudyPlan) ntnu.getStudyPlan().get(0);
		Semester semester = (Semester) studyplan.getSemester().get(1);
		assertNotNull(findDiagnostics(diagnostics, semester, "needsEnoughCourses"));
	}
	
	public void testNeedsEnoughSemesters_valid() {
		University ntnu = (University) testInstance.getContents().get(0);
		StudyPlan studyPlan = (StudyPlan) ntnu.getStudyPlan().get(0);
		assertNull(findDiagnostics(diagnostics, studyPlan, "needsEnoughSemesters"));
	}
	
	public void testNeedsEnoughSemesters_invalid() {
		University ntnu = (University) testInstance.getContents().get(0);
		StudyPlan studyPlan = (StudyPlan) ntnu.getStudyPlan().get(1);
		assertNotNull(findDiagnostics(diagnostics, studyPlan, "needsEnoughSemesters"));
	}
	
	
}