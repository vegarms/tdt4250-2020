/**
 */
package tdt4250.sp.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;
import tdt4250.sp.Semester;
import tdt4250.sp.SpFactory;
import tdt4250.sp.Specialization;
import tdt4250.sp.StudyPlan;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Study Plan</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link tdt4250.sp.StudyPlan#chooseSpecialization(tdt4250.sp.Specialization) <em>Choose Specialization</em>}</li>
 *   <li>{@link tdt4250.sp.StudyPlan#addSemester(tdt4250.sp.Semester) <em>Add Semester</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class StudyPlanTest extends TestCase {

	/**
	 * The fixture for this Study Plan test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StudyPlan fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(StudyPlanTest.class);
	}

	/**
	 * Constructs a new Study Plan test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StudyPlanTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Study Plan test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(StudyPlan fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Study Plan test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StudyPlan getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(SpFactory.eINSTANCE.createStudyPlan());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link tdt4250.sp.StudyPlan#chooseSpecialization(tdt4250.sp.Specialization) <em>Choose Specialization</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.sp.StudyPlan#chooseSpecialization(tdt4250.sp.Specialization)
	 * @generated NOT
	 */
	public void testChooseSpecialization__Specialization() {
		StudyPlan studyplan = SpFactory.eINSTANCE.createStudyPlan();
		studyplan.setStudentName("Vegard Sporstl");
		
		Semester semester = SpFactory.eINSTANCE.createSemester();
		semester.setSemesterNumber(1);
		studyplan.addSemester(semester);
		
		Specialization specialization = SpFactory.eINSTANCE.createSpecialization();
		specialization.setName("Datateknologi");
		
		studyplan.chooseSpecialization(specialization);
		
		assertTrue(studyplan.getSpecialization().contains(specialization));
	}

	/**
	 * Tests the '{@link tdt4250.sp.StudyPlan#addSemester(tdt4250.sp.Semester) <em>Add Semester</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.sp.StudyPlan#addSemester(tdt4250.sp.Semester)
	 * @generated NOT
	 */
	public void testAddSemester__Semester() {
		StudyPlan studyplan = SpFactory.eINSTANCE.createStudyPlan();
		studyplan.setStudentName("Vegard Sporst�l");
		
		Semester semester = SpFactory.eINSTANCE.createSemester();
		semester.setSemesterNumber(1);
		studyplan.addSemester(semester);
		
		assertTrue(studyplan.getSemester().contains(semester));
	}
} //StudyPlanTest
