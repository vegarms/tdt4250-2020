/**
 */
package tdt4250.sp.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import tdt4250.sp.SpFactory;
import tdt4250.sp.courseGroup;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>course Group</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class courseGroupTest extends TestCase {

	/**
	 * The fixture for this course Group test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected courseGroup fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(courseGroupTest.class);
	}

	/**
	 * Constructs a new course Group test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public courseGroupTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this course Group test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(courseGroup fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this course Group test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected courseGroup getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(SpFactory.eINSTANCE.createcourseGroup());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //courseGroupTest
