/**
 */
package tdt4250.sp;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Study Plan</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.sp.StudyPlan#getStudentName <em>Student Name</em>}</li>
 *   <li>{@link tdt4250.sp.StudyPlan#getSemester <em>Semester</em>}</li>
 *   <li>{@link tdt4250.sp.StudyPlan#getStudyProgram <em>Study Program</em>}</li>
 *   <li>{@link tdt4250.sp.StudyPlan#getSpecialization <em>Specialization</em>}</li>
 * </ul>
 *
 * @see tdt4250.sp.SpPackage#getStudyPlan()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='needsEnoughSemesters'"
 * @generated
 */
public interface StudyPlan extends EObject {
	/**
	 * Returns the value of the '<em><b>Student Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Student Name</em>' attribute.
	 * @see #setStudentName(String)
	 * @see tdt4250.sp.SpPackage#getStudyPlan_StudentName()
	 * @model
	 * @generated
	 */
	String getStudentName();

	/**
	 * Sets the value of the '{@link tdt4250.sp.StudyPlan#getStudentName <em>Student Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Student Name</em>' attribute.
	 * @see #getStudentName()
	 * @generated
	 */
	void setStudentName(String value);

	/**
	 * Returns the value of the '<em><b>Semester</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250.sp.Semester}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Semester</em>' containment reference list.
	 * @see tdt4250.sp.SpPackage#getStudyPlan_Semester()
	 * @model containment="true" required="true" upper="10"
	 * @generated
	 */
	EList<Semester> getSemester();

	/**
	 * Returns the value of the '<em><b>Study Program</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Study Program</em>' reference.
	 * @see #setStudyProgram(StudyProgram)
	 * @see tdt4250.sp.SpPackage#getStudyPlan_StudyProgram()
	 * @model
	 * @generated
	 */
	StudyProgram getStudyProgram();

	/**
	 * Sets the value of the '{@link tdt4250.sp.StudyPlan#getStudyProgram <em>Study Program</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Study Program</em>' reference.
	 * @see #getStudyProgram()
	 * @generated
	 */
	void setStudyProgram(StudyProgram value);

	/**
	 * Returns the value of the '<em><b>Specialization</b></em>' reference list.
	 * The list contents are of type {@link tdt4250.sp.Specialization}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Specialization</em>' reference list.
	 * @see tdt4250.sp.SpPackage#getStudyPlan_Specialization()
	 * @model
	 * @generated
	 */
	EList<Specialization> getSpecialization();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void chooseSpecialization(Specialization specialization);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void addSemester(Semester semester);

} // StudyPlan
