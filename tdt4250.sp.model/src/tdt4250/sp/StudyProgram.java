/**
 */
package tdt4250.sp;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Study Program</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.sp.StudyProgram#getName <em>Name</em>}</li>
 *   <li>{@link tdt4250.sp.StudyProgram#getTotalSemesters <em>Total Semesters</em>}</li>
 *   <li>{@link tdt4250.sp.StudyProgram#getSpecialization <em>Specialization</em>}</li>
 * </ul>
 *
 * @see tdt4250.sp.SpPackage#getStudyProgram()
 * @model
 * @generated
 */
public interface StudyProgram extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see tdt4250.sp.SpPackage#getStudyProgram_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link tdt4250.sp.StudyProgram#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Total Semesters</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Total Semesters</em>' attribute.
	 * @see #setTotalSemesters(int)
	 * @see tdt4250.sp.SpPackage#getStudyProgram_TotalSemesters()
	 * @model
	 * @generated
	 */
	int getTotalSemesters();

	/**
	 * Sets the value of the '{@link tdt4250.sp.StudyProgram#getTotalSemesters <em>Total Semesters</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Total Semesters</em>' attribute.
	 * @see #getTotalSemesters()
	 * @generated
	 */
	void setTotalSemesters(int value);

	/**
	 * Returns the value of the '<em><b>Specialization</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250.sp.Specialization}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Specialization</em>' containment reference list.
	 * @see tdt4250.sp.SpPackage#getStudyProgram_Specialization()
	 * @model containment="true"
	 * @generated
	 */
	EList<Specialization> getSpecialization();

} // StudyProgram
