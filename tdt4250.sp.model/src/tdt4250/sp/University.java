/**
 */
package tdt4250.sp;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>University</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.sp.University#getName <em>Name</em>}</li>
 *   <li>{@link tdt4250.sp.University#getStudyPlan <em>Study Plan</em>}</li>
 *   <li>{@link tdt4250.sp.University#getStudyProgram <em>Study Program</em>}</li>
 *   <li>{@link tdt4250.sp.University#getCourse <em>Course</em>}</li>
 *   <li>{@link tdt4250.sp.University#getCourseGroup <em>Course Group</em>}</li>
 * </ul>
 *
 * @see tdt4250.sp.SpPackage#getUniversity()
 * @model
 * @generated
 */
public interface University extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see tdt4250.sp.SpPackage#getUniversity_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link tdt4250.sp.University#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Study Plan</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250.sp.StudyPlan}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Study Plan</em>' containment reference list.
	 * @see tdt4250.sp.SpPackage#getUniversity_StudyPlan()
	 * @model containment="true"
	 * @generated
	 */
	EList<StudyPlan> getStudyPlan();

	/**
	 * Returns the value of the '<em><b>Study Program</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250.sp.StudyProgram}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Study Program</em>' containment reference list.
	 * @see tdt4250.sp.SpPackage#getUniversity_StudyProgram()
	 * @model containment="true"
	 * @generated
	 */
	EList<StudyProgram> getStudyProgram();

	/**
	 * Returns the value of the '<em><b>Course</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250.sp.Course}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course</em>' containment reference list.
	 * @see tdt4250.sp.SpPackage#getUniversity_Course()
	 * @model containment="true"
	 * @generated
	 */
	EList<Course> getCourse();

	/**
	 * Returns the value of the '<em><b>Course Group</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250.sp.courseGroup}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course Group</em>' containment reference list.
	 * @see tdt4250.sp.SpPackage#getUniversity_CourseGroup()
	 * @model containment="true"
	 * @generated
	 */
	EList<courseGroup> getCourseGroup();

} // University
