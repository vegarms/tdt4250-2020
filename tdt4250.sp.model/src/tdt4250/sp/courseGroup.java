/**
 */
package tdt4250.sp;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>course Group</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.sp.courseGroup#getName <em>Name</em>}</li>
 *   <li>{@link tdt4250.sp.courseGroup#getSpecialization <em>Specialization</em>}</li>
 *   <li>{@link tdt4250.sp.courseGroup#getCourse <em>Course</em>}</li>
 *   <li>{@link tdt4250.sp.courseGroup#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see tdt4250.sp.SpPackage#getcourseGroup()
 * @model
 * @generated
 */
public interface courseGroup extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see tdt4250.sp.SpPackage#getcourseGroup_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link tdt4250.sp.courseGroup#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Specialization</b></em>' reference list.
	 * The list contents are of type {@link tdt4250.sp.Specialization}.
	 * It is bidirectional and its opposite is '{@link tdt4250.sp.Specialization#getCourses <em>Courses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Specialization</em>' reference list.
	 * @see tdt4250.sp.SpPackage#getcourseGroup_Specialization()
	 * @see tdt4250.sp.Specialization#getCourses
	 * @model opposite="courses"
	 * @generated
	 */
	EList<Specialization> getSpecialization();

	/**
	 * Returns the value of the '<em><b>Course</b></em>' reference list.
	 * The list contents are of type {@link tdt4250.sp.Course}.
	 * It is bidirectional and its opposite is '{@link tdt4250.sp.Course#getGroup <em>Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course</em>' reference list.
	 * @see tdt4250.sp.SpPackage#getcourseGroup_Course()
	 * @see tdt4250.sp.Course#getGroup
	 * @model opposite="group"
	 * @generated
	 */
	EList<Course> getCourse();

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link tdt4250.sp.groupType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see tdt4250.sp.groupType
	 * @see #setType(groupType)
	 * @see tdt4250.sp.SpPackage#getcourseGroup_Type()
	 * @model
	 * @generated
	 */
	groupType getType();

	/**
	 * Sets the value of the '{@link tdt4250.sp.courseGroup#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see tdt4250.sp.groupType
	 * @see #getType()
	 * @generated
	 */
	void setType(groupType value);

} // courseGroup
