/**
 */
package tdt4250.sp;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>group Type</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see tdt4250.sp.SpPackage#getgroupType()
 * @model
 * @generated
 */
public enum groupType implements Enumerator {
	/**
	 * The '<em><b>Obligatorisk</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OBLIGATORISK_VALUE
	 * @generated
	 * @ordered
	 */
	OBLIGATORISK(0, "Obligatorisk", "O"),

	/**
	 * The '<em><b>Valgbart A</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #VALGBART_A_VALUE
	 * @generated
	 * @ordered
	 */
	VALGBART_A(1, "ValgbartA", "VA"),

	/**
	 * The '<em><b>Valgbart B</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #VALGBART_B_VALUE
	 * @generated
	 * @ordered
	 */
	VALGBART_B(2, "ValgbartB", "VB"),

	/**
	 * The '<em><b>MAX1A</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MAX1A_VALUE
	 * @generated
	 * @ordered
	 */
	MAX1A(3, "MAX1A", "MAX1A"),

	/**
	 * The '<em><b>Minst1</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MINST1_VALUE
	 * @generated
	 * @ordered
	 */
	MINST1(4, "Minst1", "M1A"),

	/**
	 * The '<em><b>Minst2</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MINST2_VALUE
	 * @generated
	 * @ordered
	 */
	MINST2(5, "Minst2", "M2A");

	/**
	 * The '<em><b>Obligatorisk</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OBLIGATORISK
	 * @model name="Obligatorisk" literal="O"
	 * @generated
	 * @ordered
	 */
	public static final int OBLIGATORISK_VALUE = 0;

	/**
	 * The '<em><b>Valgbart A</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #VALGBART_A
	 * @model name="ValgbartA" literal="VA"
	 * @generated
	 * @ordered
	 */
	public static final int VALGBART_A_VALUE = 1;

	/**
	 * The '<em><b>Valgbart B</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #VALGBART_B
	 * @model name="ValgbartB" literal="VB"
	 * @generated
	 * @ordered
	 */
	public static final int VALGBART_B_VALUE = 2;

	/**
	 * The '<em><b>MAX1A</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MAX1A
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int MAX1A_VALUE = 3;

	/**
	 * The '<em><b>Minst1</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MINST1
	 * @model name="Minst1" literal="M1A"
	 * @generated
	 * @ordered
	 */
	public static final int MINST1_VALUE = 4;

	/**
	 * The '<em><b>Minst2</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MINST2
	 * @model name="Minst2" literal="M2A"
	 * @generated
	 * @ordered
	 */
	public static final int MINST2_VALUE = 5;

	/**
	 * An array of all the '<em><b>group Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final groupType[] VALUES_ARRAY =
		new groupType[] {
			OBLIGATORISK,
			VALGBART_A,
			VALGBART_B,
			MAX1A,
			MINST1,
			MINST2,
		};

	/**
	 * A public read-only list of all the '<em><b>group Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<groupType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>group Type</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static groupType get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			groupType result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>group Type</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static groupType getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			groupType result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>group Type</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static groupType get(int value) {
		switch (value) {
			case OBLIGATORISK_VALUE: return OBLIGATORISK;
			case VALGBART_A_VALUE: return VALGBART_A;
			case VALGBART_B_VALUE: return VALGBART_B;
			case MAX1A_VALUE: return MAX1A;
			case MINST1_VALUE: return MINST1;
			case MINST2_VALUE: return MINST2;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private groupType(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //groupType
