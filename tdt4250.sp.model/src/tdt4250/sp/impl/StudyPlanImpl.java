/**
 */
package tdt4250.sp.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import tdt4250.sp.Semester;
import tdt4250.sp.SpPackage;
import tdt4250.sp.Specialization;
import tdt4250.sp.StudyPlan;
import tdt4250.sp.StudyProgram;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Study Plan</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.sp.impl.StudyPlanImpl#getStudentName <em>Student Name</em>}</li>
 *   <li>{@link tdt4250.sp.impl.StudyPlanImpl#getSemester <em>Semester</em>}</li>
 *   <li>{@link tdt4250.sp.impl.StudyPlanImpl#getStudyProgram <em>Study Program</em>}</li>
 *   <li>{@link tdt4250.sp.impl.StudyPlanImpl#getSpecialization <em>Specialization</em>}</li>
 * </ul>
 *
 * @generated
 */
public class StudyPlanImpl extends MinimalEObjectImpl.Container implements StudyPlan {
	/**
	 * The default value of the '{@link #getStudentName() <em>Student Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStudentName()
	 * @generated
	 * @ordered
	 */
	protected static final String STUDENT_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getStudentName() <em>Student Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStudentName()
	 * @generated
	 * @ordered
	 */
	protected String studentName = STUDENT_NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSemester() <em>Semester</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSemester()
	 * @generated
	 * @ordered
	 */
	protected EList<Semester> semester;

	/**
	 * The cached value of the '{@link #getStudyProgram() <em>Study Program</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStudyProgram()
	 * @generated
	 * @ordered
	 */
	protected StudyProgram studyProgram;

	/**
	 * The cached value of the '{@link #getSpecialization() <em>Specialization</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecialization()
	 * @generated NOT
	 * @ordered
	 */
	protected EList<Specialization> specialization;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StudyPlanImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SpPackage.Literals.STUDY_PLAN;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getStudentName() {
		return studentName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setStudentName(String newStudentName) {
		String oldStudentName = studentName;
		studentName = newStudentName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpPackage.STUDY_PLAN__STUDENT_NAME, oldStudentName, studentName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Semester> getSemester() {
		if (semester == null) {
			semester = new EObjectContainmentEList<Semester>(Semester.class, this, SpPackage.STUDY_PLAN__SEMESTER);
		}
		return semester;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public StudyProgram getStudyProgram() {
		if (studyProgram != null && studyProgram.eIsProxy()) {
			InternalEObject oldStudyProgram = (InternalEObject)studyProgram;
			studyProgram = (StudyProgram)eResolveProxy(oldStudyProgram);
			if (studyProgram != oldStudyProgram) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SpPackage.STUDY_PLAN__STUDY_PROGRAM, oldStudyProgram, studyProgram));
			}
		}
		return studyProgram;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StudyProgram basicGetStudyProgram() {
		return studyProgram;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setStudyProgram(StudyProgram newStudyProgram) {
		StudyProgram oldStudyProgram = studyProgram;
		studyProgram = newStudyProgram;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpPackage.STUDY_PLAN__STUDY_PROGRAM, oldStudyProgram, studyProgram));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Specialization> getSpecialization() {
		if (specialization == null) {
			specialization = new EObjectResolvingEList<Specialization>(Specialization.class, this, SpPackage.STUDY_PLAN__SPECIALIZATION);
		}
		return specialization;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public void chooseSpecialization(Specialization specialization) {
		this.getSpecialization().add(specialization);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return 
	 * @generated NOT
	 */
	@Override
	public void addSemester(Semester semester) {
		this.getSemester().add(semester);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SpPackage.STUDY_PLAN__SEMESTER:
				return ((InternalEList<?>)getSemester()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SpPackage.STUDY_PLAN__STUDENT_NAME:
				return getStudentName();
			case SpPackage.STUDY_PLAN__SEMESTER:
				return getSemester();
			case SpPackage.STUDY_PLAN__STUDY_PROGRAM:
				if (resolve) return getStudyProgram();
				return basicGetStudyProgram();
			case SpPackage.STUDY_PLAN__SPECIALIZATION:
				return getSpecialization();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SpPackage.STUDY_PLAN__STUDENT_NAME:
				setStudentName((String)newValue);
				return;
			case SpPackage.STUDY_PLAN__SEMESTER:
				getSemester().clear();
				getSemester().addAll((Collection<? extends Semester>)newValue);
				return;
			case SpPackage.STUDY_PLAN__STUDY_PROGRAM:
				setStudyProgram((StudyProgram)newValue);
				return;
			case SpPackage.STUDY_PLAN__SPECIALIZATION:
				getSpecialization().clear();
				getSpecialization().addAll((Collection<? extends Specialization>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SpPackage.STUDY_PLAN__STUDENT_NAME:
				setStudentName(STUDENT_NAME_EDEFAULT);
				return;
			case SpPackage.STUDY_PLAN__SEMESTER:
				getSemester().clear();
				return;
			case SpPackage.STUDY_PLAN__STUDY_PROGRAM:
				setStudyProgram((StudyProgram)null);
				return;
			case SpPackage.STUDY_PLAN__SPECIALIZATION:
				getSpecialization().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SpPackage.STUDY_PLAN__STUDENT_NAME:
				return STUDENT_NAME_EDEFAULT == null ? studentName != null : !STUDENT_NAME_EDEFAULT.equals(studentName);
			case SpPackage.STUDY_PLAN__SEMESTER:
				return semester != null && !semester.isEmpty();
			case SpPackage.STUDY_PLAN__STUDY_PROGRAM:
				return studyProgram != null;
			case SpPackage.STUDY_PLAN__SPECIALIZATION:
				return specialization != null && !specialization.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case SpPackage.STUDY_PLAN___CHOOSE_SPECIALIZATION__SPECIALIZATION:
				chooseSpecialization((Specialization)arguments.get(0));
				return null;
			case SpPackage.STUDY_PLAN___ADD_SEMESTER__SEMESTER:
				addSemester((Semester)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (studentName: ");
		result.append(studentName);
		result.append(')');
		return result.toString();
	}

} //StudyPlanImpl
