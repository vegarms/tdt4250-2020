/**
 */
package tdt4250.sp.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import tdt4250.sp.Course;
import tdt4250.sp.SpPackage;
import tdt4250.sp.StudyPlan;
import tdt4250.sp.StudyProgram;
import tdt4250.sp.University;
import tdt4250.sp.courseGroup;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>University</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.sp.impl.UniversityImpl#getName <em>Name</em>}</li>
 *   <li>{@link tdt4250.sp.impl.UniversityImpl#getStudyPlan <em>Study Plan</em>}</li>
 *   <li>{@link tdt4250.sp.impl.UniversityImpl#getStudyProgram <em>Study Program</em>}</li>
 *   <li>{@link tdt4250.sp.impl.UniversityImpl#getCourse <em>Course</em>}</li>
 *   <li>{@link tdt4250.sp.impl.UniversityImpl#getCourseGroup <em>Course Group</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UniversityImpl extends MinimalEObjectImpl.Container implements University {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getStudyPlan() <em>Study Plan</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStudyPlan()
	 * @generated
	 * @ordered
	 */
	protected EList<StudyPlan> studyPlan;

	/**
	 * The cached value of the '{@link #getStudyProgram() <em>Study Program</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStudyProgram()
	 * @generated
	 * @ordered
	 */
	protected EList<StudyProgram> studyProgram;

	/**
	 * The cached value of the '{@link #getCourse() <em>Course</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourse()
	 * @generated
	 * @ordered
	 */
	protected EList<Course> course;

	/**
	 * The cached value of the '{@link #getCourseGroup() <em>Course Group</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourseGroup()
	 * @generated
	 * @ordered
	 */
	protected EList<courseGroup> courseGroup;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UniversityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SpPackage.Literals.UNIVERSITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpPackage.UNIVERSITY__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<StudyPlan> getStudyPlan() {
		if (studyPlan == null) {
			studyPlan = new EObjectContainmentEList<StudyPlan>(StudyPlan.class, this, SpPackage.UNIVERSITY__STUDY_PLAN);
		}
		return studyPlan;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<StudyProgram> getStudyProgram() {
		if (studyProgram == null) {
			studyProgram = new EObjectContainmentEList<StudyProgram>(StudyProgram.class, this, SpPackage.UNIVERSITY__STUDY_PROGRAM);
		}
		return studyProgram;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Course> getCourse() {
		if (course == null) {
			course = new EObjectContainmentEList<Course>(Course.class, this, SpPackage.UNIVERSITY__COURSE);
		}
		return course;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<courseGroup> getCourseGroup() {
		if (courseGroup == null) {
			courseGroup = new EObjectContainmentEList<courseGroup>(courseGroup.class, this, SpPackage.UNIVERSITY__COURSE_GROUP);
		}
		return courseGroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SpPackage.UNIVERSITY__STUDY_PLAN:
				return ((InternalEList<?>)getStudyPlan()).basicRemove(otherEnd, msgs);
			case SpPackage.UNIVERSITY__STUDY_PROGRAM:
				return ((InternalEList<?>)getStudyProgram()).basicRemove(otherEnd, msgs);
			case SpPackage.UNIVERSITY__COURSE:
				return ((InternalEList<?>)getCourse()).basicRemove(otherEnd, msgs);
			case SpPackage.UNIVERSITY__COURSE_GROUP:
				return ((InternalEList<?>)getCourseGroup()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SpPackage.UNIVERSITY__NAME:
				return getName();
			case SpPackage.UNIVERSITY__STUDY_PLAN:
				return getStudyPlan();
			case SpPackage.UNIVERSITY__STUDY_PROGRAM:
				return getStudyProgram();
			case SpPackage.UNIVERSITY__COURSE:
				return getCourse();
			case SpPackage.UNIVERSITY__COURSE_GROUP:
				return getCourseGroup();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SpPackage.UNIVERSITY__NAME:
				setName((String)newValue);
				return;
			case SpPackage.UNIVERSITY__STUDY_PLAN:
				getStudyPlan().clear();
				getStudyPlan().addAll((Collection<? extends StudyPlan>)newValue);
				return;
			case SpPackage.UNIVERSITY__STUDY_PROGRAM:
				getStudyProgram().clear();
				getStudyProgram().addAll((Collection<? extends StudyProgram>)newValue);
				return;
			case SpPackage.UNIVERSITY__COURSE:
				getCourse().clear();
				getCourse().addAll((Collection<? extends Course>)newValue);
				return;
			case SpPackage.UNIVERSITY__COURSE_GROUP:
				getCourseGroup().clear();
				getCourseGroup().addAll((Collection<? extends courseGroup>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SpPackage.UNIVERSITY__NAME:
				setName(NAME_EDEFAULT);
				return;
			case SpPackage.UNIVERSITY__STUDY_PLAN:
				getStudyPlan().clear();
				return;
			case SpPackage.UNIVERSITY__STUDY_PROGRAM:
				getStudyProgram().clear();
				return;
			case SpPackage.UNIVERSITY__COURSE:
				getCourse().clear();
				return;
			case SpPackage.UNIVERSITY__COURSE_GROUP:
				getCourseGroup().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SpPackage.UNIVERSITY__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case SpPackage.UNIVERSITY__STUDY_PLAN:
				return studyPlan != null && !studyPlan.isEmpty();
			case SpPackage.UNIVERSITY__STUDY_PROGRAM:
				return studyProgram != null && !studyProgram.isEmpty();
			case SpPackage.UNIVERSITY__COURSE:
				return course != null && !course.isEmpty();
			case SpPackage.UNIVERSITY__COURSE_GROUP:
				return courseGroup != null && !courseGroup.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //UniversityImpl
